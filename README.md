# Stub README for toolexample

**DO NOT LEAVE THIS README UNPOPULATED**

This project contains toolexample. This tool is intended to ...

## Installation

You can install toolexample from the GitLab package registry:

```console
pip3 install toolexample \
  --index-url https://gitlab.developers.cam.ac.uk/api/v4/groups/5/-/packages/pypi/simple
```

If you run into permissions issues, create a [personal access
token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with
the `read_api` scope and add the following to `~/.pypirc`:

```ini
[distutils]
index-servers =
    gitlab

[gitlab]
repository = https://gitlab.developers.cam.ac.uk/api/v4/projects/404/packages/pypi
username = __token__
password = {your personal access token}
```
