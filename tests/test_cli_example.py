import toolexample


def test_cli_example_subcommand(set_cli_args):
    """Calling the example subcommand with no options succeeds."""
    set_cli_args("toolexample", "example-subcommand")
    toolexample.main()
