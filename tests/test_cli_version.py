import importlib.metadata

import toolexample


def test_cli_version(set_cli_args, capsys):
    """Passing --version on the command line prints out the package version."""
    set_cli_args("toolexample", "--version")
    expected_version = importlib.metadata.version("toolexample")
    toolexample.main()
    assert capsys.readouterr().out.strip() == expected_version
