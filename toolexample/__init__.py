"""
Example command-line tool.

Usage:
    {cmd} (-h | --help)
    {cmd} --version
    {cmd} example-subcommand [--example-option=NUMBER]

Options:
    -h, --help                  Show a brief usage summary.
    --version                   Show which version of {cmd} is installed.

Example subcommand:
    --example-option=NUMBER     An example option
"""
import importlib.metadata
import os
import pprint
import sys

import docopt


def main():
    opts = docopt.docopt(__doc__.format(cmd=os.path.basename(sys.argv[0])))

    # Print package version if --version flag specified.
    if opts["--version"]:
        print(importlib.metadata.version(__name__))
        return

    # Pretty-print command line options.
    print("command line options:")
    pprint.pprint(opts)
